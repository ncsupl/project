# GROUP A

## Usage
Run `python main.py` in the [proj3/src/](proj3/src/) directory. For more usage information, run
`python main.py -h` or see below.

```
usage: main.py [-h] [-b NUM_BOTS] [-g {bartok,memory}] [-n NUM_PLAYERS]
               [-p PLAYERS [PLAYERS ...]] [-s SEED]

optional arguments:
  -h, --help            show this help message and exit
  -b NUM_BOTS, --num_bots NUM_BOTS
                        Number of bots
  -g {bartok,memory}, --game {bartok,memory}
                        Game to play
  -n NUM_PLAYERS, --num_players NUM_PLAYERS
                        Number of human players
  -p PLAYERS [PLAYERS ...], --players PLAYERS [PLAYERS ...]
                        List of player names
  -s SEED, --seed SEED  Randomizer seed
```


## Authors
Galen Abell

Daxis Agarwal

Abeer Altaf