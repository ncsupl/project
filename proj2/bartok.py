from mar1.deck import Deck
from mar1.player import Player
from mar1.card import Card
from mar1.util import cards


class Bartok(object):
    def __init__(self, players):
        self.deck = Deck([Card(i) for i in cards])
        self.players = [Player(p) for p in players]
        self.discard_pile = Deck()
        self.winner = None

    def play(self):
        self.deal_cards()
        # place top card from deck to start the discard pile
        self.discard_pile.play(self.deck.draw())
        p = 0

        while True:
            current_player = p % len(self.players)  # index of the current player
            print('---------- Turn: %s ----------' % self.players[current_player].name)
            top_card = self.discard_pile.peek()[0]
            print('Board: (D) %s; Hand: %s' % (str(top_card), self.players[current_player].hand_str()))
            option = input('> ')
            if option == 'D':
                # pick a card from the draw pile
                self.players[current_player].hand.play(self.deck.draw())
            else:
                # Pop the nth card from the players hand, where n is the option they chose
                self.discard_pile.play(self.players[current_player].hand.draw(int(option)))

            if self.players[current_player].hand.size() == 0:
                self.winner = self.players[current_player]
                break
            if self.end():
                break
            p += 1

        print('Player %s wins!' % self.winner.name if self.winner is not None else 'Everybody loses!')

    def deal_cards(self):
        """
        Deals 5 cards to each player
        """
        for _ in range(0, 5):
            for p in self.players:
                p.hand.play(self.deck.draw())

    def end(self):
        """
        Game ends when the deck is empty.
        """
        return self.deck.size() == 0
