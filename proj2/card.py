# cards: ♥♦♠♣
card_types = {
    "A": 1,
    "J": 11,
    "Q": 12,
    "K": 13,
}


class Card(object):
    def __init__(self, card):
        self.text = card
        self.rank = card_types[card[0]] if card[0] in card_types else card[0]
        self.suit = card[1]

    def __str__(self):
        return self.text

    def __eq__(self, other):
        return other.rank == self.rank and other.suit == self.suit

    def is_suit(self, other):
        return other.suit == self.suit

    def is_rank(self, other):
        return other.rank == self.rank

# ace_spades = Card(1, "spades")
# ace_other = Card(1, "hearts")
# print("Same Card", ace_spades.compare(ace_other))
# print("Same 0", ace_spades.isrank(ace_other))
# print(Card("K♥"))
