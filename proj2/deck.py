import random


class Deck(object):
    """
    Represents a Deck of Cards.
    """

    def __init__(self, cards=None, seed=1):
        """
        Initializes the Deck with the given seed.
        :param seed: The seed for generating random results (default to 1).
        :param cards: The list of Cards to use (default to []).
        """
        self.cards = cards or []
        random.seed(seed)

    def __str__(self):
        """
        Returns a string representation of the Deck.
        :return: A list of string representations for each Card in the Deck.
        """
        return ' '.join([str(c) for c in self.cards])

    def draw(self, n=0):
        """
        Draw a card from the Deck.
        :param n: The card index to draw.
        :return: The card drawn from the Deck.
        """
        return self.cards.pop(n)

    def empty(self):
        """
        Checks if the Deck is empty.
        :return: True if the Deck is empty, else false.
        """
        return self.size() == 0

    def peek(self, n=1):
        """
        Peeks at the top n Cards.
        :param n: The number of Cards to peek (default to 1).
        :return: A sublist of cards on top of the Deck.
        """
        return self.cards[:n]

    def play(self, card):
        """
        Adds a Card to the Deck.
        :param card: The Card to add.
        """
        self.cards.insert(0, card)

    def shuffle(self):
        """
        Shuffles the Deck.
        """
        random.shuffle(self.cards)

    def size(self):
        """
        Gets the size of the Deck.
        :return: The size of the Deck.
        """
        return len(self.cards)
