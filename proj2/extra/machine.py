from mar1.util import Object, NonLocalException
from .state import State, match_state


class Machine(Object):
    """
    General purpose state machine.
    """

    def __init__(self, **kwargs):
        """
        Initializes the Machine with the given list of arguments, empty dictionary of States, and empty starting State.
        :param kwargs: List of optional arguments.
        """
        super().__init__(**kwargs)
        self.states = {}
        self.start = None

    def state(self, state, **kwargs):
        """
        Adds a State to the Machine.
        :param state: State to match.
        :param kwargs: List of optional arguments.
        :return: The created State.
        """
        self.states[state] = curr = self.states[state] if state in self.states else self._state(state, **kwargs)
        self.start = self.start or curr
        return curr

    def _state(self, state, **kwargs):
        """
        Creates a new State based on the given parameters.
        :param state: State to match.
        :param kwargs: List of optional arguments.
        :return: The created State.
        """
        if isinstance(state, State):
            return state
        for i in match_state(State):
            if i.tag and state.find(i.tag) != -1:
                return i(self, **kwargs)
        return State(self, **kwargs)

    def trans(self, here, guard, there):
        """
        Adds a transition to the Machine's States.
        :param here: The starting State.
        :param guard: The guard.
        :param there: The ending State.
        """
        self.state(here).trans(guard, self.state(there))

    def run(self):
        """
        Run the Machine, catching NonLocalExceptions.
        """
        try:
            self._run()
        except NonLocalException as e:
            print('Caught NonLocalException: ' + str(e))

    def _run(self):
        """
        Run the Machine to completion, or until an Exception is thrown.
        :return: The value of the last State's on_exit() method.
        """
        state = self.start
        state.on_entry()
        while True:
            state = state.step()
            if state.exit():
                break
        return state.on_exit()
