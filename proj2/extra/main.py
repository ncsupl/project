import sys
from .logger import Logger

if __name__ == '__main__':
    debug = '--debug' in sys.argv
    logger = Logger(debug=debug)
    print('main')
