from mar1.util import Object


class State(Object):
    """
    General purpose state machine state.
    """
    tag = ""

    def __init__(self, machine, **kwargs):
        """
        Initialize the State with the given State Machine, list of optional arguments, and empty list of transitions.
        :param machine: State Machine containing this State.
        :param kwargs: List of optional arguments.
        """
        super().__init__(**kwargs)
        self.machine = machine
        self._trans = []

    def __str__(self):
        """
        Get the State's tag.
        :return: The State's tag.
        """
        return self.tag

    def trans(self, guard, there):
        """
        Add a transition to this State's list of transitions.
        :param guard: The guard.
        :param there: The next State.
        """
        self._trans += [Object(guard=guard, there=there)]

    def step(self):
        """
        Step to the next State.
        :return: The next State, or the current State if no guards were satisfied.
        """
        for trans in self._trans:
            if trans.guard():
                self.on_exit()
                trans.there.on_entry()
                return trans.there
        return self

    def on_entry(self):
        """
        Default on_entry method.
        """
        pass

    def on_exit(self):
        """
        Default on_exit method.
        """
        pass

    def exit(self):
        """
        Check if the Machine should exit (default false).
        """
        return False


class End(State):
    """
    Represents an ending State in a State machine.
    """
    tag = "end"

    def exit(self):
        """
        The machine should exit.
        """
        return True


class NestedState(State):
    """
    Represents a State containing a State Machine.
    """

    def __init__(self, inner, **kwargs):
        """
        Initialize the State
        :param inner: Inner State Machine to run.
        :param kwargs: List of optional arguments.
        """
        super().__init__(**kwargs)
        self.inner = inner

    def on_entry(self):
        """
        Run the inner State Machine on State entry.
        """
        super().on_entry()
        self.machine.run()


def match_state(tag, seen=None):
    """
    Finds the appropriate State based on the given tag.
    :param tag: State's tag to match.
    :param seen: List of States that have been seen already.
    :return: Yields a list of possible States.
    """
    seen = seen or set()
    if tag not in seen:
        seen.add(tag)
        yield tag
        for i in tag.__subclasses__():
            for j in match_state(i, seen):
                yield j
