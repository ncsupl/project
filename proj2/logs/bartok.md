# Game 1
```
Select game: (M)emory or (B)artok
> B
---------- Turn: Player 1 ----------
Board: (D) JS; Hand: (0) 9S, (1) 7S, (2) 5S, (3) 3S, (4) AS
> 0
---------- Turn: Player 2 ----------
Board: (D) 9S; Hand: (0) 10S, (1) 8S, (2) 6S, (3) 4S, (4) 2S
> 0
---------- Turn: Player 1 ----------
Board: (D) 10S; Hand: (0) 7S, (1) 5S, (2) 3S, (3) AS
> 0
---------- Turn: Player 2 ----------
Board: (D) 7S; Hand: (0) 8S, (1) 6S, (2) 4S, (3) 2S
> 0
---------- Turn: Player 1 ----------
Board: (D) 8S; Hand: (0) 5S, (1) 3S, (2) AS
> 0
---------- Turn: Player 2 ----------
Board: (D) 5S; Hand: (0) 6S, (1) 4S, (2) 2S
> 0
---------- Turn: Player 1 ----------
Board: (D) 6S; Hand: (0) 3S, (1) AS
> 0
---------- Turn: Player 2 ----------
Board: (D) 3S; Hand: (0) 4S, (1) 2S
> 0
---------- Turn: Player 1 ----------
Board: (D) 4S; Hand: (0) AS
> 0
Player Player 1 wins!
```


# Game 2
```
Select game: (M)emory or (B)artok
> B
---------- Turn: Player 1 ----------
Board: (D) JS; Hand: (0) 9S, (1) 7S, (2) 5S, (3) 3S, (4) AS
> D
---------- Turn: Player 2 ----------
Board: (D) JS; Hand: (0) 10S, (1) 8S, (2) 6S, (3) 4S, (4) 2S
> 0
---------- Turn: Player 1 ----------
Board: (D) 10S; Hand: (0) QS, (1) 9S, (2) 7S, (3) 5S, (4) 3S, (5) AS
> 0
---------- Turn: Player 2 ----------
Board: (D) QS; Hand: (0) 8S, (1) 6S, (2) 4S, (3) 2S
> 0
---------- Turn: Player 1 ----------
Board: (D) 8S; Hand: (0) 9S, (1) 7S, (2) 5S, (3) 3S, (4) AS
> 0
---------- Turn: Player 2 ----------
Board: (D) 9S; Hand: (0) 6S, (1) 4S, (2) 2S
> 0
---------- Turn: Player 1 ----------
Board: (D) 6S; Hand: (0) 7S, (1) 5S, (2) 3S, (3) AS
> 0
---------- Turn: Player 2 ----------
Board: (D) 7S; Hand: (0) 4S, (1) 2S
> 0
---------- Turn: Player 1 ----------
Board: (D) 4S; Hand: (0) 5S, (1) 3S, (2) AS
> 0
---------- Turn: Player 2 ----------
Board: (D) 5S; Hand: (0) 2S
> 0
Player Player 2 wins!
```
