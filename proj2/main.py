from mar1.memory import Memory
from mar1.bartok import Bartok

games = {
    'M': lambda: Memory(['Player 1', 'Player 2']).play(),
    'B': lambda: Bartok(['Player 1', 'Player 2']).play()
}

if __name__ == '__main__':
    print('Select game: (M)emory or (B)artok')
    games[input('> ')]()
