from mar1.deck import Deck
from mar1.player import Player
from mar1.card import Card
from mar1.util import cards

class Memory(object):
    def __init__(self, players):
        self.deck = [Deck(c) for c in [[Card(i)] for i in cards]]
        self.players = [Player(p) for p in players]

    def play(self):
        p = 0
        while True:
            print('---------- Turn: %s ----------' % self.players[p % len(self.players)].name)
            self.print_board()
            # x1, y1 = None, None
            # while x1 is not None and y1 is not None:
            x1, y1 = (int(x) - 1 for x in input('Please choose first card (x, y): ').split(' '))
            # x2, y2 = None, None
            # while x2 is not None and y2 is not None:
            x2, y2 = (int(x) - 1 for x in input('Please choose second card (x, y): ').split(' '))
            self.print_board([(x1, y1), (x2, y2)])
            c1 = self.card_at(x1, y1).peek(1)[0]
            c2 = self.card_at(x2, y2).peek(2)[0]
            if c1.rank == c2.rank:
                self.set_card(x1, y1, None)
                self.set_card(x2, y2, None)
                self.players[p % len(self.players)].hand.play(c1)
                self.players[p % len(self.players)].hand.play(c2)
            if self.end():
                break
            p += 1
        winner = self.winner()
        print('Player %s wins!' % winner[1].name) if winner is not None else print('You both lose! BOO!')

    def print_board(self, c=None):
        """
        Prints the board. Prints a [] for overturned cards, * for missing cards, or actual card values if they are in c.
        :param c: A list of cards to show.
        """
        c = c or []
        # print the header numbers
        print('  ', end='')
        for x in range(0, 13):
            print('%2d' % (x + 1), end=' ')
        print()
        for i in range(0, 4):
            print(i + 1, end=' ')
            for j in range(0, 13):
                if (i, j) in c:
                    print(self.card_at(i, j), end=' ')
                else:
                    print('[]' if self.card_at(i, j) is not None else '* ', end=' ')
            print()

    def card_at(self, x, y):
        return self.deck[x * 13 + y]

    def set_card(self, x, y, c):
        self.deck[x * 13 + y] = c

    def winner(self):
        """
        Gets the winner, or None if the game is tied.
        :return: Returns a tuple with the player's hand size as the first argument and the player as the second.
        """
        values = sorted([(p.hand.size(), p) for p in self.players], key=lambda x: x[0])
        return None if values[0][0] == values[1][0] else values[0]

    def end(self):
        return all(c is None for c in self.deck)

