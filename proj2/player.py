from mar1.util import Object
from mar1.deck import Deck


class Player(Object):
    def __init__(self, name, hand=None, **kwargs):
        super().__init__(**kwargs)
        self.hand = hand or Deck()
        self.name = name

    def hand_str(self):
        return ', '.join(['(%d) %s' % pair for pair in enumerate(self.hand.peek(self.hand.size()))])
