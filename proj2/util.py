"""
General purpose utility classes and functions.
"""
cards = ['AS', '2S', '3S', '4S', '5S', '6S', '7S', '8S', '9S', '10S', 'JS', 'QS', 'KS',
         'AD', '2D', '3D', '4D', '5D', '6D', '7D', '8D', '9D', '10D', 'JD', 'QD', 'KD',
         'AC', '2C', '3C', '4C', '5C', '6C', '7C', '8C', '9C', '10C', 'JC', 'QC', 'KC',
         'AH', '2H', '3H', '4H', '5H', '6H', '7H', '8H', '9H', '10H', 'JH', 'QH', 'KH']


class Object(object):
    """
    General purpose object.
    """

    def __init__(self, **kwargs):
        """
        Initialize Object with arbitrary state.
        :param kwargs: State to add to Object.
        """
        self.__dict__.update(kwargs)

    def __getitem__(self, key):
        """
        Allows usage of self.key to get state / methods instead of self['key'].
        :param key: The name of the key.
        :return: The value of key.
        """
        return self.__dict__[key]

    def __str__(self):
        """
        Get a human-readable representation of the object.
        :return: String representation of the object.
        """
        return self.__class__.__name__ + kv(self.__dict__)


class NonLocalException(Exception):
    # we're an exception, yay
    pass


def kv(d):
    """
    Get string representation of key-value pairs for a dictionary.
    :param d: The dictionary to parse.
    :return: String representation of the dictionary's key-value pairs.
    """
    return '(%s)'.format(', '.join(['%s: %s'.format(k, d[k]) for k in sorted(d.keys()) if k[0] != '_']))
