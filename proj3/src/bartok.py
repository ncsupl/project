from card import Card, cards
from deck import Deck
from game import Game, make_game, ctx
from player import Player
from util import o, true


##### HELPER METHODS #####
def can_play():
    return len(valid_cards()) > 0


def valid_cards():
    return [str((i, c)[0]) for i, c in enumerate(ctx.player().hand) if
        c.is_suit(ctx.discard.first()) or
        c.is_rank(ctx.discard.first())]


def input_parser(i):
    return [i]


def any_parser(i):
    return ['ok']


##### GAME METHODS #####
def setup(num_bots, num_players, players, seed):
    def setup():
        ctx.deck = Deck([Card(c) for c in cards], seed=seed).shuffle()
        ctx.discard = Deck([ctx.deck.draw()])
        ctx.num_players = num_players
        ctx.players = [
            Player(
                'Player {}'.format(p),
                Deck([ctx.deck.draw() for _ in range(5)])
            ) for p in (range(num_players) if len(players) == 0 else players)
        ]
    return setup


def end():
    return len([p for p in ctx.players if p.hand.empty()]) > 0 or ctx.deck.empty()


def winner():
    return [p for p in ctx.players if p.hand.empty()][0]
##### GAME METHODS #####


##### TURN METHODS #####
def get_action():
    ctx.ui.clear()
    ctx.ui.broadcast('D: %s; H: %s' % (ctx.discard.first(), ctx.player().hand))
    if can_play():
        ctx.action = ctx.ui.get_input('play or draw? ',
                                      'please choose \'play\' or \'draw\'',
                                      ['play', 'draw'],
                                      input_parser)
    else:
        # we still need to wait for input, so just accept anything
        ctx.ui.get_input('press enter to draw ',
                         '',
                         ['ok'],
                         any_parser)
        ctx.action = ['draw']


def is_play():
    return ctx.action == ['play']


def is_draw():
    return ctx.action == ['draw']


def play():
    pick = int(ctx.ui.get_input(
        'choose card: ',
        'please select a card in your hand with the same suit or rank',
        valid_cards(),
        input_parser
    )[0])
    ctx.discard.play(ctx.player().hand.draw(pick))
    ctx.action = None


def draw():
    ctx.player().hand.play(ctx.deck.draw())
    ctx.action = None
##### TURN METHODS #####


##### TURN SPEC #####
turn = [
    o(
        trigger=true,
        action=get_action,
        next=[
            o(
                trigger=is_play,
                action=play,
                next=None
            ),
            o(
                trigger=is_draw,
                action=draw,
                next=None
            )
        ]
    )
]
##### TURN SPEC #####


##### GAME SPEC #####
def bartok(num_bots, num_players, players, seed):
    ctx.name = 'bartok'
    ctx.setup = setup(num_bots, num_players, players, seed)
    ctx.winner = winner
    ctx.end = end
    ctx.turn = turn
    make_game().run()
##### GAME SPEC #####
