# cards: ♥♦♠♣
card_types = {
    "A": 1,
    "J": 11,
    "Q": 12,
    "K": 13,
}

cards = ['AS', '2S', '3S', '4S', '5S', '6S', '7S', '8S', '9S', '10S', 'JS', 'QS', 'KS',
          'AD', '2D', '3D', '4D', '5D', '6D', '7D', '8D', '9D', '10D', 'JD', 'QD', 'KD',
          'AC', '2C', '3C', '4C', '5C', '6C', '7C', '8C', '9C', '10C', 'JC', 'QC', 'KC',
          'AH', '2H', '3H', '4H', '5H', '6H', '7H', '8H', '9H', '10H', 'JH', 'QH', 'KH']

# @formatter:off
#cards = [
#    'A\u2660', '2\u2660', '3\u2660', '4\u2660', '5\u2660', '6\u2660', '7\u2660', '8\u2660', '9\u2660', '10\u2660', 'J\u2660', 'Q\u2660', 'K\u2660',  # spades
#    'A\u2666', '2\u2666', '3\u2666', '4\u2666', '5\u2666', '6\u2666', '7\u2666', '8\u2666', '9\u2666', '10\u2666', 'J\u2666', 'Q\u2666', 'K\u2666',  # diamonds
#    'A\u2663', '2\u2663', '3\u2663', '4\u2663', '5\u2663', '6\u2663', '7\u2663', '8\u2663', '9\u2663', '10\u2663', 'J\u2663', 'Q\u2663', 'K\u2663',  # clubs
#    'A\u2665', '2\u2665', '3\u2665', '4\u2665', '5\u2665', '6\u2665', '7\u2665', '8\u2665', '9\u2665', '10\u2665', 'J\u2665', 'Q\u2665', 'K\u2665'   # hearts
#]
# @formatter:on


class Card(object):
    def __init__(self, card):
        self.text = card
        self.rank = card_types[card[:-1]] if card[:-1] in card_types else card[:-1]
        self.suit = card[-1]

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return self.text

    def __eq__(self, other):
        return other.rank == self.rank and other.suit == self.suit

    def is_suit(self, other):
        return other.suit == self.suit

    def is_rank(self, other):
        return other.rank == self.rank
