from machine import Machine
from state import State, Exit, NestedState
from turn import make_turn
from ui import UI
from util import o, true

ctx = o(
    current_player=0,
    deck=None,
    discard=None,
    players=None,
    num_players=-1,
    ui=UI(),
    player=lambda: ctx.players[ctx.current_player],
    setup=None,
    winner=None,
    end=None
)


class Game(Machine):
    pass

# game agnostic states
class Start(State):
    tag = "start"

    def _on_entry(self):
        ctx.ui.broadcast('Starting game: %s' % ctx.name)
        ctx.setup()


class Turn(NestedState):
    tag = "turn"

    def _on_entry(self):
        ctx.ui.broadcast('{}\'s Turn'.format(ctx.player().name))
        super()._on_entry()

    def _on_exit(self):
        ctx.current_player = (ctx.current_player + 1) % ctx.num_players


class End(Exit):
    tag = "end"

    def _on_entry(self):
        w = ctx.winner()
        ctx.ui.broadcast('{} wins'.format(w.name) if w else 'Game tied')


# @formatter:off
def spec(m, s, t, n):
    start = s("start")
    turn  = s("turn", n)
    end   = s("end")

    t(start,    true,  turn)  # start always goes to turn
    t( turn, ctx.end,   end)  # if the game is over, end
    t( turn,    true,  turn)  # otherwise go back to turn
# @formatter:on


def make_game():
    g = Game(ctx.name)
    spec(g, g.state, g.trans, make_turn('turn', ctx.turn))
    return g
