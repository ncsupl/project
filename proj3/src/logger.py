from time import strftime, localtime


class Logger:
    """
    Util for logging information.
    """

    def __init__(self, debug=False, output='output.txt'):
        """
        Initializes the Logger with the given debug info and output file.
        :param debug: Whether the program is in debug mode (will print all debug and error logs to stdout).
        :param output: Output log file.
        """
        self.debug = debug
        self.output = output

    def log(self, string, prompt):
        """
        Logs the given string prepended with the timestamp and prompt to the output file.
        :param string: The string to log.
        :param prompt: The prompt to prepend.
        """
        print('(%s) %s %s'.format(self.now(), prompt, string), file=open(self.output))

    def log_out(self, string):
        """
        Logs text to stdout and logfile.
        :param string: The string to log.
        """
        print(string)
        self.log(string, '>')

    def log_debug(self, string):
        """
        Logs a debug statement to stdout if debug mode, and logfile.
        :param string: The string to log.
        """
        if self.debug:
            print(string)
        self.log(string, '%')

    def log_err(self, string):
        """
        Logs an error to stdout if debug mode, and logfile.
        :param string: The string to log.
        """
        if self.debug:
            print(string)
        self.log(string, '#')

    @staticmethod
    def now():
        """
        Gets the current time in a nicely formatted string.
        :return: String representation of localtime.
        """
        return strftime('%a, %d %b %Y %H:%M:%S', localtime())
