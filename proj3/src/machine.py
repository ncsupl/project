from exception import NonLocalException
from state import State
from util import isa, contains


class Machine(object):
    def __init__(self, name):
        self.states = {}
        self.name = name
        self.start = None

    def isa(self, x, m=None):
        if isinstance(x, State):
            return x
        for k in isa(State):
            if k.tag and contains(x, k.tag):
                return k(x, self, m)
        return State(x, self, m)

    def state(self, x, m=None):
        # self.states[x] = y = self.isa(x, m)
        self.states[x] = y = self.states[x] if x in self.states else self.isa(x, m)
        self.start = self.start or y
        return y

    def trans(self, here, guard, there):
        self.state(here).trans(guard, self.state(there))

    def run(self):
        try:
            self._run()
        except NonLocalException as e:
            print('Caught NonLocalException: ' + str(e))

    def _run(self):
        #print('booting %s' % self.name)
        state = self.start
        state.on_entry()
        for _ in range(1000000):
            state = state.step()
            if state.quit():
                break
        return state.on_exit()

    # helper methods for always taking or ignoring a transition
    @staticmethod
    def true():
        return True

    @staticmethod
    def false():
        return False
