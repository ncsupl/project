import sys
import argparse

from bartok import bartok
from memory import memory
from util import o


games = {
    'bartok': bartok,
    'memory': memory
}


def load_args():
    parser = argparse.ArgumentParser()

    names = [g for g in games.keys()]
    parser.add_argument('-b', '--num_bots', help='Number of bots', default=0)
    parser.add_argument('-g', '--game', help='Game to play',
            default=names[0], choices=names)
    parser.add_argument('-n', '--num_players', help='Number of human players',
            default=2, type=int)
    parser.add_argument('-p', '--players', help='List of player names',
            default=[], type=str, nargs='+')
    parser.add_argument('-s', '--seed', help='Randomizer seed', default=1,
            type=int)

    return parser.parse_args()


if __name__ == '__main__':
    args = load_args()
    if len(args.players) > 0 and len(args.players) != args.num_players:
        print('Player names must match the number of players')
        exit(1)
    games[args.game](
        args.num_bots,
        args.num_players,
        args.players,
        args.seed
    )

