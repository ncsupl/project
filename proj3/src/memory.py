import re

from ast import literal_eval as as_tuple

from card import Card, cards
from deck import Deck
from game import Game, make_game, ctx
from player import Player
from util import o, true


##### HELPER METHODS #####
def flatten_pos(x, y):
    return x * 13 + y


def flatten_card(x, y):
    return ctx.deck.at(flatten_pos(x, y))


def expand_card(n):
    return (n % 13, n // 13)


def draw_board(c=None):
    c = c or []
    ret = '  '
    for x in range(0, 13):
        ret += '%-3d ' % x
    ret += '\n'
    for i in range(0, 4):
        ret += '{} '.format(i)
        for j in range(0, 13):
            if (i, j) in c:
                ret += '%-3s ' % flatten_card(i, j)
            else:
                ret += '[]  ' if flatten_card(i, j) is not None else '*   '
        ret += '\n'
    return ret


def valid_cards():
    valid = []
    for i in range(0, 4):
        for j in range(0, 13):
            if flatten_card(i, j) is not None:
                valid += [(str(i), str(j))]
    return valid


def input_parser(i):
    try:
        nums = re.findall('\d+', i)
        return [(nums[0], nums[1]), (nums[2], nums[3])]
    except Exception:
        return []
##### HELPER METHODS #####


##### GAME METHODS #####
def setup(num_bots, num_players, players, seed):
    def setup():
        ctx.deck = Deck([Card(c) for c in cards], seed=seed).shuffle()
        ctx.num_players = num_players
        ctx.players = [
            Player(
                'Player {}'.format(p),
                Deck()
            ) for p in (range(num_players) if len(players) == 0 else players)
        ]
    return setup


def winner():
    p = sorted([p for p in ctx.players], key=lambda x: x.hand.size())
    return p[0] if p[0].hand.size() != p[1].hand.size() else None


def end():
    return ctx.deck.empty()
##### GAME METHODS #####


##### TURN METHODS #####
def get_draw():
    ctx.ui.clear()
    ctx.ui.broadcast(draw_board())
    raw = ctx.ui.get_input(
        'choose cards as (x1,y1) (x2,y2) ',
        'please choose two valid cards',
        valid_cards(),
        input_parser
    )
    p1, p2 = ((int(x), int(y)) for x, y in raw)
    c1, c2 = (flatten_card(c[0], c[1]) for c in (p1, p2))
    ctx.ui.broadcast(draw_board([p1, p2]))
    if c1.is_rank(c2):
        ctx.ui.get_input('That\'s a match! ',
                         '',
                         ['ok'],
                         lambda i: ['ok'])
        ctx.deck.set(None, flatten_pos(p1[0], p1[1]))
        ctx.deck.set(None, flatten_pos(p2[0], p2[1]))
        ctx.player().hand.play(c1)
        ctx.player().hand.play(c2)
    else:
        ctx.ui.broadcast('Cards didn\'t match')
##### TURN METHODS #####


##### TURN SPEC #####
turn = [
    o(
        trigger=true,
        action=get_draw,
        next=None
    )
]
##### TURN SPEC #####


##### GAME SPEC #####
def memory(num_bots, num_players, players, seed):
    ctx.name = 'memory'
    ctx.setup = setup(num_bots, num_players, players, seed)
    ctx.winner = winner
    ctx.end = end
    ctx.turn = turn
    make_game().run()
##### GAME SPEC #####
