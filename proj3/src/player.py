from deck import Deck


class Player(object):
    def __init__(self, name, hand=None):
        self.hand = hand or Deck()
        self.name = name

