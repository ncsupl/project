from util import o

"""
Some generic states for the Game machine
"""


class State(object):
    tag = ""

    def __init__(self, name, m, i):
        self.name = name
        self._trans = []
        self.m = m
        self.i = i

    def __repr__(self):
        return 'n: {}, t: {}'.format(self.name, self.m, self.i, self._trans)

    def trans(self, guard, there):
        self._trans += [o(guard=guard, there=there)]

    def step(self):
        for j in self._trans:
            if j.guard():
                #print("now", j.guard.__name__)
                self.on_exit()
                j.there.on_entry()
                return j.there
        return self

    def on_entry(self):
        #print("arriving at {}".format(self.name if self.name != '' else str(self)))
        self._on_entry()

    def _on_entry(self):
        pass

    def on_exit(self):
        #print("leaving %s" % self.name)
        self._on_exit()

    def _on_exit(self):
        pass

    def quit(self):
        return False


class Exit(State):
    tag = "."

    def quit(self):
        return True

    def _on_exit(self):
        return self


class NestedState(State):
    tag = "nest"

    def _on_entry(self):
        super()._on_entry()
        self.i.run()
