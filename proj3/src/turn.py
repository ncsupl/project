import uuid

from machine import Machine
from util import true


def parse_action(a, s, t):
    i = s(str(uuid.uuid4()))  # generate a unique, random name for the internal states so we don't have overlap
    i._on_entry = a.action

    for n in a.next or range(0):  # if we have next states, build them as well, otherwise skip this
        t(i, n.trigger, parse_action(n, s, t))
    t(i, true, s('.'))  # default next is always exits
    return i


def spec(s, t, a):
    start = s('s')
    for action in a:
        t(start, action.trigger, parse_action(action, s, t))


def make_turn(n, a):
    m = Machine(n)
    spec(m.state, m.trans, a)
    return m
