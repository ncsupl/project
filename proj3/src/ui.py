import os
import re


class UI(object):
    def __init__(self):
        self.clear()

    @staticmethod
    def clear():
        os.system('cls' if os.name == 'nt' else 'clear')

    @staticmethod
    def get_input(prompt, error, must, parser):
        result = parser(input(prompt).strip())
        while not (len(result) != 0 and all(x in must for x in result)):
            print(error)
            result = parser(input(prompt).strip())
        return result

    @staticmethod
    def broadcast(message):
        print(message)
