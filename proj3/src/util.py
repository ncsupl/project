class o(object):
    def __init__(self, **dic):
        self.__dict__.update(dic)

    def __repr__(self):
        return self.__class__.__name__ + kv(self.__dict__)

    def __getitem__(self, x):
        return self.__dict__[x]

    def __iadd__(self, x):
        self.__dict__.update(x)


def contains(all, some):
    return all.find(some) != -1


def isa(k, seen=None):
    assert isinstance(k, type), "superclass must be 'object'"
    seen = seen or set()
    if k not in seen:
        seen.add(k)
        yield k
        for sub in k.__subclasses__():
            for x in isa(sub, seen):
                yield x


def kv(d):
    """Return a string of the dictionary,
       keys in sorted order,
       hiding any key that starts with '_'"""
    return '(' + ', '.join(['%s: %s' % (k, d[k])
                            for k in sorted(d.keys())
                            if k[0] != "_"]) + ')'


def true():
    return True


def false():
    return False
